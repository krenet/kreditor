package krenet.bim.editor.test.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class KrebimTestController {

	@RequestMapping(value = "/index.do", produces="application/json;charset=UTF-8")
	public String index(){
		return "index";
	}
}
