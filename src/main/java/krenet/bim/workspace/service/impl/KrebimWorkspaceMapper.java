package krenet.bim.workspace.service.impl;

import java.util.ArrayList;

import egovframework.rte.psl.dataaccess.mapper.Mapper;
import krenet.bim.workspace.service.KrebimWorkspaceVO;

@Mapper("krebimWorkspaceMapper")
public interface KrebimWorkspaceMapper {
	
	public ArrayList<KrebimWorkspaceVO> getWorkspaceList();

}
