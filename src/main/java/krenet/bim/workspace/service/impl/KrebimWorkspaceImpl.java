package krenet.bim.workspace.service.impl;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import krenet.bim.workspace.service.KrebimWorkspaceService;
import krenet.bim.workspace.service.KrebimWorkspaceVO;

@Service("krebimWorkspaceService")
public class KrebimWorkspaceImpl implements KrebimWorkspaceService {

	@Resource(name="krebimWorkspaceMapper")
	private KrebimWorkspaceMapper krebimWorkspaceMapper;
	
	@Override
	public ArrayList<KrebimWorkspaceVO> getWorkspaceList() {
		
		ArrayList<KrebimWorkspaceVO> workspaceList = krebimWorkspaceMapper.getWorkspaceList();
		
		if(workspaceList.size()==0) {
			return null;
		}else {
			return workspaceList;
		}
	}

}
