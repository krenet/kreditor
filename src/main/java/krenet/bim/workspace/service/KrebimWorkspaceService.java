package krenet.bim.workspace.service;

import java.util.ArrayList;

public interface KrebimWorkspaceService {

	public ArrayList<KrebimWorkspaceVO> getWorkspaceList();
}
