package krenet.bim.workspace.web;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import krenet.bim.workspace.service.KrebimWorkspaceService;
import krenet.bim.workspace.service.KrebimWorkspaceVO;

@Controller
public class KrebimWorkspaceController {
	
	@Resource(name="krebimWorkspaceService")
	private KrebimWorkspaceService krebimWorkspaceService;

	@RequestMapping(value = "/getWorkspaceList.do", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public @ResponseBody ArrayList<KrebimWorkspaceVO> getWorkspaceList() {
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		mav.addObject("test", "test");
//		
//		System.out.println("여기는 들어오는가");
//		
////		mav.addObject("res", tpmsRoadInfoService.selectNameList());
//		return  mav;
		
		ArrayList<KrebimWorkspaceVO> workspaceList = krebimWorkspaceService.getWorkspaceList();
		
		if(workspaceList.size()==0) {
			 return null;
		}else {
			return workspaceList;
		}
		
	}
	
	
}
